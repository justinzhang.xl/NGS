#/bin/bash
#$ -M justinzhang.xl@lanl.gov
#$ -m abe
#$ -pe smp 20
#$ -l h_vmem=10G
#$ -j y
#$ -V
#$ -cwd
#$ -S /bin/bash

#: '
unset LOADEDMODULES; unset _LMFILES_

source /etc/profile.d/modules.sh
module purge
module load shared default-environment
module load blast
module load bowtie/2.1.0
module load EUK
 
module unload java
module load java/1.8.0_111
module unload python
module load python/2.7.9
module unload bamtools
module load bamtools
module load braker/2.1.0
module unload perl
module unload perl_lib
module unload perl
module unload perl_lib
module load perl/5.16.3
module load perl_lib/5.16.3

/users/justinzhang/app/miniconda3/bin/

export UNIPROT_BLAST_DB="/users/justinzhang/324184/algae_annotation_pipeline/Eukaryote_genome_annotation/uniprot/uniprot_sprot.fasta"
export UNIPROT_FA="/users/justinzhang/324184/algae_annotation_pipeline/Eukaryote_genome_annotation/uniprot/uniprot_sprot.fasta"

which bamtools

#export PATH=$PATH:/opt/apps/GeneMark-ET-4.32/gm_et_linux_64/gmes_petap/
which gmes_petap.pl
which braker.pl
#'


for var in "$@"
do	
	#echo "$var"
	if [ `echo $var | cut -c 1` == '-' ];
	then
		next_arg="$var"
	else
		case "$next_arg" in
			-g)
				genome_fasta="$var"
				echo "genome_fasta $genome_fasta"
				;;
			-trimmed)
				trimmed="$var"
				;;
			-r1)
				read1="$var"
				echo "read 1 $read1"
				;;
			-r2)
				read2="$var"
				echo "read2 $read2"
				;;
			-d)
				annotation_folder="$var"
				echo "annotation folder $annotation_folder"
				mkdir -p $annotation_folder
				;;
			-locus_tag)
				locus_tag="$var"
				echo "gene locus tag - $locus_tag"
				;;
		esac
	fi
done

echo "$trimmed"
if [ $trimmed ];
then
	fastq1=$read1
	fastq2=$read2
fi

echo "/users/justinzhang/app/install_tophat/bin/tophat --num-threads 20 -o $tophat_output $bindex $fastq1,$fastq2"

#require paired end
if [ ! -f $genome_fasta ];
then
	echo "please input reference genome fasta file "
	echo "Usage: sh braker_annotation.sh -locus_tag gene_locus_tag -d annotation_directory -g reference_genome_fasta_file -r1 fastq_read1_file -r2 fastq_read2_file"
	exit
fi

if [ ! $read1 ] && [ ! $read2 ]; 
then
	echo "input fastq files as following format"
	echo "-r1 PE_reads_1.fq.gz -r2 PE_reads_2.fq.gz"
	exit
fi
#checking file existing - later consideration

if [ $read1 ] && [ $read2 ];
then
	if [ ! -f $read1 ];
	then
		echo "fastq file read 1 $read1 does not exist"
		echo "Usage: sh braker_annotation.sh -locus_tag gene_locus_tag -d annotation_directory -g reference_genome_fasta_file -r1 fastq_read1_file -r2 fastq_read2_file"
		exit
	fi
	if [ ! -f $read2 ];
	then
		echo "fastq file read 1 $read2 does not exist"
		echo "Usage: sh braker_annotation.sh -locus_tag gene_locus_tag -d annotation_directory -g reference_genome_fasta_file -r1 fastq_read1_file -r2 fastq_read2_file"
		exit
	fi
fi

if [ $annotation_folder = "" ];
then
	echo "please input genome annotation folder "
	echo "Usage: sh braker_annotation.sh -locus_tag gene_locus_tag -d annotation_directory -g reference_genome_fasta_file -r1 fastq_read1_file -r2 fastq_read2_file"
	exit
fi
if [ ! -d $annotation_folder ]; 
then
	mkdir -p $annotation_folder
	echo "could not create the genome annotation folder - $annotation_folder"
	echo "Usage: sh braker_annotation.sh -locus_tag gene_locus_tag -d annotation_directory -g reference_genome_fasta_file -r1 fastq_read1_file -r2 fastq_read2_file"
	exit
fi

if [ $locus_tag = "" ];
then
	echo "please input gene locus tag"
	echo "Usage: sh braker_annotation.sh -locus_tag gene_locus_tag -d annotation_directory -g reference_genome_fasta_file -r1 fastq_read1_file -r2 fastq_read2_file"
	exit
fi

#parsing input

#simplifying headers of reference fasta file - this is required by BRAKER alignment bam file

echo "simplifying headers of reference fasta file"
echo "genome fasta $genome_fasta"
ref_folder=`dirname $genome_fasta`
echo "reference folder $ref_folder"
genome_fasta_file=`basename $genome_fasta`
simple_header_fasta=$ref_folder"/simple_header_"$genome_fasta_file
echo "simple header fasta $simple_header_fasta"
if [ ! -f $simple_header_fasta ];
then
	python simplify_fasta_header.py $genome_fasta $simple_header_fasta
fi
simple_header_fasta=$genome_fasta

#input assembly or genome name
genome=`basename $genome_fasta | cut -d '.' -f 1`
prefix=$genome

#annotation_folder="braker_annotation_"$genome"_"$aligner
#annotation_folder="braker_annotation_"$genome
#mkdir -p $annotation_folder

#statistics

perl assemblathon_stats.pl $simple_header_fasta -csv -graph > $annotation_folder"/output_assemblathon_stats.txt"

#Step 1 preprocessing reads

#bam_folder=fold_Chlamydomonas/trimmed_read/FaQCs_trimmed_read

#read1="Tetraselmis_hajni/MMETSP0817-Tetraselmis-sp--.1.fastq.gz"
fastq_name=`basename $read1`

#pre_trimmed_read=Chlamydomonas_FaQCs_trimmed
pre_trimmed_read=$fastq_name

bam_folder=`dirname $read1`"/FaQCs_trimmed_read"
#bam_folder="Tetraselmis_alignment/FaQCs_trimmed_read"
mkdir -p $bam_folder

if [ "$fastq1" != "$read1" ];
then
	fastq1=$bam_folder/$pre_trimmed_read.1.trimmed.fastq 
	fastq2=$bam_folder/$pre_trimmed_read.2.trimmed.fastq

	echo "fastq1 $fastq1"

	if [ ! -f $fastq1 ];
	then 
		echo "Step 1: preprocessing reads using FaQCs tool"

		/users/justinzhang/app/miniconda3/bin/FaQCs --prefix $pre_trimmed_read -t 10 -d $bam_folder --adapter -1 $read1 -2 $read2
		if [ -f $fastq1 ];
		then
			touch $annotation_folder/.done_trimming.txt
		fi
	fi
fi
#FaQCs --prefix $pre_trimmed_read -t 10 -d $bam_folder --adapter -1 fold_Chlamydomonas/test_1.fastq -2 fold_Chlamydomonas/test_2.fastq
#FaQCs --prefix $pre_trimmed_read -t 10 -d $bam_folder --adapter -1 $read1 -2 $read2

#Step 2 alignment using tophat

tophat_output=$annotation_folder/"tophat_output_"$prefix
mkdir -p $tophat_output
sortedbam=$tophat_output"/accepted_hits.bam"
echo "bam file $sortedbam"

echo "tophat_output $tophat_output"

if [ ! -f $sortedbam ];
then 
	echo "mapping using tophat: bowtie"

	bindex=`echo $simple_header_fasta | rev | cut -c 5- | rev`
	bindex=$bindex"_bindex"
	num=`ls $bindex.* | wc -l`
	if [ ! $num = 6 ]; 
	then 
		which bowtie2-build
		bowtie2-build $simple_header_fasta $bindex
	fi
	echo "/users/justinzhang/app/install_tophat/bin/tophat --num-threads 20 -o $tophat_output $bindex $fastq1,$fastq2"
	/users/justinzhang/app/install_tophat/bin/tophat --num-threads 20 -o $tophat_output $bindex $fastq1,$fastq2
	if [ -f $sortedbam ];
	then 
		touch $annotation_folder/.tophat_done.txt
	fi
fi

#augustus configure - training species for BRAKER
species=$genome"_bowtie"
braker_output=$annotation_folder/"braker_annotation"
	
mkdir -p $braker_output

if [ ! -f $annotation_folder/.done_BRAKER.txt ];
then 
	echo "Step 3: annotating genome using BRAKER"
	if [ -d /opt/apps/augustus-3.3/config/species/$species ];
	then
		rm -r /opt/apps/augustus-3.3/config/species/$species
	fi

	echo "braker.pl --AUGUSTUS_BIN_PATH=/opt/apps/augustus-3.3/bin/ --workingdir=$braker_output --genome=$simple_header_fasta --bam=$sortedbam --BAMTOOLS_PATH=/opt/apps/bamtools-2.4.1/bin/ --species=$species"
	augustus_gtf=$braker_output"/augustus.hints.gtf"
	echo "current loaded modules"
	braker.pl --AUGUSTUS_BIN_PATH=/opt/apps/augustus-3.3/bin/ --workingdir=$braker_output --genome=$simple_header_fasta --bam=$sortedbam --BAMTOOLS_PATH=/opt/apps/bamtools-2.4.1/bin/ --species=$species --useexisting
	if [ -f $augustus_gtf ];
	then 
		touch $annotation_folder/.done_BRAKER.txt
	fi	
fi

#test

augustus_gtf=$braker_output"/augustus.hints.gtf"
augustus_gff=$braker_output"/augustus.hints.gff"
interproscan_folder=$annotation_folder/interproscan_annotation
mkdir -p $interproscan_folder
interproscan_aa_file=`pwd`/$interproscan_folder/$genome"_mRNA.aa"
mRNA_file=$interproscan_folder/$genome"_mRNA.fasta"
augustus_file_name=`basename $augustus_gff | rev | cut -c 5-100|rev`
#aa_file=`dirname $augustus_gff`"/"$augustus_file_name".aa"
aa_file=`echo $augustus_gff | rev | cut -c 4- | rev`"aa"
blastp_out=$interproscan_folder"/genome.proteins.blastp"


if [ ! -f $interproscan_aa_file ];
then
	echo "Extracting fasta files of proteins and coding sequencing"
	#perl /panfs/biopan01/apps/augustus-3.3/scripts/getAnnoFasta.pl $augustus_gff 2>output_getAnnoFasta.txt
	getAnnoFasta.pl $augustus_gff
	python parse_aa.py $aa_file $locus_tag
	#ln -s `pwd`/$aa_file `pwd`/$interproscan_folder/$augustus_file_name".aa"
	ln -s `pwd`"/"$aa_file"_"$locus_tag $interproscan_aa_file
fi

if [ ! -f $annotation_folder"/.done_locus_tag.txt" ];
then
	echo "Converting BRAKER augustus.gtf to gff format"
	#perl /opt/apps/Anaconda3/bin/gtf2gff.pl  <$augustus_gtf --gff3 --printExon --out=$interproscan_folder/$genome"_augustus.hints.gff_all"
	perl /opt/apps/Anaconda3/1.6.5/bin/gtf2gff.pl <$augustus_gtf --gff3 --printExon --out=$interproscan_folder/$genome"_augustus.hints.gff_all"
	egrep "gene|mRNA|CDS" $interproscan_folder/$genome"_augustus.hints.gff_all" > $interproscan_folder/$genome"_augustus.hints.gff"
	python parse_gff.py $interproscan_folder/$genome"_augustus.hints.gff" $locus_tag
	#gff file with locus tag
	#cat $simple_header_fasta >> $interproscan_folder/$genome"_augustus.hints.gff_"$locus_tag
	touch $annotation_folder"/.done_locus_tag.txt"
fi


if [ ! -f $annotation_folder/.done_interproscan.txt ];
then
	echo "Step 4: Performing functional annotation for predicted genes from BRAKER: augustus"
	mkdir -p $interproscan_folder
	echo "Performing interproscan searching for protein fasta file $interproscan_aa_file"
	ipr5_new --input $interproscan_aa_file --outdir $interproscan_folder --prefix $genome --cpu 20 2> $annotation_folder/error_iprscan5.txt
	#nohup ipr5_new --input BRAKER_Chlamydomonas_tophat_2/braker_annotation/augustus.hints.aa_CREI --outdir $interproscan_folder --prefix $genome --cpu 20 2> $annotation_folder/error_iprscan5.txt &
	touch $annotation_folder/.done_interproscan.txt
fi

#update reviewed protein database 
#wget ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz
#blastp -db /panfs/biopan01/refdb/usrdb/uniprot_sprot_BH1/uniprot_sprot.fasta -query ../genome.proteins.fasta -outfmt 6 -max_hsps_per_subject 1 -max_target_seqs 1 -seg yes -evalue 1e-6 -num_threads 10 > genome.proteins.blastp

if [ ! -f $blastp_out ];
then
	echo "blast search UniProt database"
	blastp -db $UNIPROT_BLAST_DB -query $interproscan_aa_file -outfmt 6 -max_hsps_per_subject 1 -max_target_seqs 1 -seg yes -evalue 1e-6 -num_threads 20 > $blastp_out

	#nohup blastp -db $UNIPROT_BLAST_DB -query BRAKER_Chlamydomonas_tophat_2/braker_annotation/augustus.hints.aa_CREI -outfmt 6 -max_hsps_per_subject 1 -max_target_seqs 1 -seg yes -evalue 1e-6 -num_threads 20 > $blastp_out 2> out_blast.txt &
	touch $annotation_folder/.done_blastp.txt
fi

#integrate interproscan result to gff - take a GFF3 file and an iprscan output file and add Dbxrefs and Ontology_term fields to the GFF3 attributes field.
#converted from Braker augustus gtf and extracted gene mRNA exon CDs
#braker_gff=$interproscan_folder/$genome"_augustus.hints.gff"
braker_gff=$interproscan_folder/$genome"_augustus.hints.gff_"$locus_tag
#ln -s `pwd`"/"$augustus_gff3"_"$locus_tag $braker_gff
interproscan_output=$interproscan_folder/$genome".tsv"
braker_interproscan_gff=$interproscan_folder/$genome"_interproscan_augustus.hints.gff"
braker_interproscan_blastp_gff=$interproscan_folder/$genome"_interproscan_blastp_augustus.hints.gff"

if [ ! -f $braker_interproscan_gff ];
then
	ipr5_update_gff_2018 $braker_gff $interproscan_output > $braker_interproscan_gff
fi

#make functional gff - add blastp result 
if [ ! -f $braker_interproscan_blastp_gff ];
then
	cat $simple_header_fasta >> $braker_interproscan_blastp_gff
	maker_functional_gff_new /panfs/biopan01/refdb/usrdb/uniprot_sprot_BH1/uniprot_sprot.fasta $blastp_out $braker_interproscan_gff > $braker_interproscan_blastp_gff
	#replacing locus index number
	#echo "replacing gene locus tag"
	#grep "ID=g" $braker_interproscan_blastp_gff | while read -r ln ; do rep=`echo "$ln" | cut -f 9 | cut -d ';' -f 1 | cut -d '.' -f 1`;j=`echo "$ln" | cut -f 9 | cut -d ';' -f 1 | cut -d '.' -f 1 | cut -c 5`;c=`expr 10 - $j / 10 - 1`;to=`perl -E "print '0' x $c"`$j;rep2="ID=g$to";ln2=`echo "$ln" | sed -e "s/${rep}/${rep2}/g"`;echo "$ln2" >> $braker_interproscan_blastp_gff"_2";done
	#replacing locus tag
	#sed -e "s/ID=g/ID=${locus_tag}_/g" $braker_interproscan_blastp_gff"_2" > $braker_interproscan_blastp_gff
	#extracting mRNA sequence
	#mRNA_file=$interproscan_folder/$genome"_mRNA.fasta"
	#cat $braker_interproscan_gff | gffGetmRNA.pl --genome=$simple_header_fasta --mrna=$mRNA_file
	#add fasta sequences to gff file
	#cat $simple_header_fasta >> $braker_interproscan_blastp_gff_2
	#braker_interproscan_blastp_gff=$braker_interproscan_blastp_gff"_2"
fi

	# replacing locus tag

	#grep "ID=g" BRAKER_Chlamydomonas_tophat/interproscan_annotation/GCF_000002595_interproscan_blastp_augustus.hints.gff | while read -r ln ; do rep=`echo "$ln" | cut -f 9 | cut -d ';' -f 1 | cut -d '.' -f 1`;j=`echo "$ln" | cut -f 9 | cut -d ';' -f 1 | cut -d '.' -f 1 | cut -c 5`;c=`expr 10 - $j / 10 - 1`;to=`perl -E "print '0' x $c"`$j;rep2="ID=g$to";ln2=`echo "$ln" | sed -e "s/${rep}/${rep2}/g"`;echo "$ln2" >> BRAKER_Chlamydomonas_tophat/interproscan_annotation/GCF_000002595_interproscan_blastp_augustus.hints.gff_2;done
    #grep "Parent=g" BRAKER_Chlamydomonas_tophat/interproscan_annotation/GCF_000002595_interproscan_blastp_augustus.hints.gff | while read -r ln ; do rep=`echo "$ln" | cut -f 9 | cut -d ';' -f 2 | cut -d '.' -f 1`;j=`echo "$ln" | cut -f 9 | cut -d ';' -f 2 | cut -d '.' -f 1 | cut -c 9`;c=`expr 10 - $j / 10 - 1`;to=`perl -E "print '0' x $c"`$j;rep2="Parent=CREI_$to";ln2=`echo "$ln" | sed -e "s/${rep}/${rep2}/g"`;echo "$ln2" >> BRAKER_Chlamydomonas_tophat/interproscan_annotation/GCF_000002595_interproscan_blastp_augustus.hints.gff_3;done
	#cat BRAKER_Chlamydomonas_tophat/interproscan_annotation/genome.proteins.blastp | while read -r ln ; do rep=`echo "$ln" | cut -f 1 | cut -d '.' -f 1`;j=`echo "$ln" | cut -f 1 | cut -d '.' -f 1 | cut -c 2`;c=`expr 10 - $j / 10 - 1`;to=`perl -E "print '0' x $c"`$j;rep2="CREI_$to";ln2=`echo "$ln" | sed -e "s/${rep}/${rep2}/g"`;echo "$ln2" >> BRAKER_Chlamydomonas_tophat/interproscan_annotation/genome.proteins.blastp_2;done

	#adding fasta sequence to gff file



#Generating KEGG pathway 

kegg_folder=$annotation_folder"/kegg_folder"
mkdir -p $kegg_folder
if [ ! -f $kegg_folder/info_ec.json ];
then
	/users/justinzhang/app/omics-pathway-viewer/scripts/opaver_anno_2.pl -g $braker_interproscan_blastp_gff --outdir $kegg_folder
fi

	#visualize kegg pathway from mac
	#python -m SimpleHTTPServer 8000 open "http://localhost:8000/pathway_anno.html?data=Chlamydomonas"
	#http://edge-dev-master.lanl.gov/opaver_web/pathway_anno.html?data=324184_KEGG/kegg_Chlamydomonas 

	#Make JBrowse tracking files
	#maker2jbrowse [OPTION] <gff3file1> <gff3file2> ...
	
jbrowse_folder=$annotation_folder"/jbrowse_folder"
mkdir -p $jbrowse_folder

#mv jbrowse_Chlamydomonas /panfs/biopan01/scratch-324184/Jbrowser/
	#chmod -R g+rx jbrowse_Chlamydomonas
	#visualize from EDGE http://edge-dev-master.lanl.gov/JBrowse/?data=data/324184_Jbrowser/jbrowse_Chlamydomonas


#add sequences to gff file
if [ ! -f $jbrowse_folder/trackList.json ]
then
	#require memory
	perl braker_maker2jbrowse_parallel.pl -c 10 -o $jbrowse_folder $braker_interproscan_blastp_gff 2> output_braker_maker2jbrowse_parallel.txt
fi

#GAG output generating fasta files of protein, mRNA
if [ ! -d $annotation_folder"/gag_output" ];
then
	python gag.py --fasta $simple_header_fasta --gff $braker_interproscan_blastp_gff --out $annotation_folder"/gag_output"
fi

#sed -i 's/Pfam/PFAM/g' gff file
#NCBI submission
#create bioproject and biosamples
#create template file
#~/324184/scratch/app/linux64.table2asn_GFF -M n -J -c w -euk -t template.sbt -gaps-min 10 -l paired-ends -i NSK_all_genomes.fasta -f NSK_BRAKER_2.gff -locus-tag-prefix NSK -o NSK.sqn -Z NSK.dr -j "[organism=Nannochloropsis salina] [strain=CCMP1776]"
#check NSK.val NSK.stats for errors
#remove genes with errors, ignore warnings

exit

#NCBI genome publication https://www.ncbi.nlm.nih.gov/genbank/genomes_gff/

#/home/justinzhang/324184/scratch/algae_annotation/tbl2asn

#ncbi new script to take input of gff file 

#change Note= to product= for mRNA/CDS as product name

#~/app/linux.tbl2asn -M n -J -c w -t template.sbt -gaps-min 10 -l paired-ends -j "[organism=Auxenochlorella protothecoides] [strain=UTEX 25]" -i CPI_nuclear_mito_chlo_rewrite.fsa -f APUTEX25.maker_only_rename.gff -o ap.sqn -Z discrep


functional_annotation_folder=$annotation_folder/functional_annotation
mkdir -p $functional_annotation_folder
echo "Extracting fasta files of proteins and coding sequencing"
perl /panfs/biopan01/apps/augustus-3.3/scripts/getAnnoFasta.pl $augustus_gff 2>output_getAnnoFasta.txt
